package middleware

import (
	"encoding/json"

	"ebdm.dev/battleships/v1/lib"
	"ebdm.dev/battleships/v1/models"
	"github.com/gin-gonic/gin"
)

func FetchGame() gin.HandlerFunc {
	return func(c *gin.Context) {
		var input models.Game
		id, _ := c.Params.Get("game")
		err := json.NewDecoder(c.Request.Body).Decode(&input)
		lib.HandleError(err)
		game, err := models.GetGame(id)
		lib.HandleError(err)
		c.Set("game", game)
		c.Next()
	}
}