package controllers

import (
	"encoding/json"

	"ebdm.dev/battleships/v1/lib"
	"ebdm.dev/battleships/v1/models"
	"github.com/gin-gonic/gin"
	"github.com/imdario/mergo"
)

func ParseGame(c *gin.Context) (*models.Game, error) {
	var game models.Game
	err := json.NewDecoder(c.Request.Body).Decode(&game)
	return &game, err
}

func NewGame(c *gin.Context) {
	game := models.NewGame()
	c.JSON(200, game)
}

func GetGame(c *gin.Context) {
	game := c.MustGet("game").(*models.Game)
	c.JSON(200, game)
}

func GetAllGames(c *gin.Context) {
	games := models.GetAllGames()
	c.JSON(200, games)
}

func PutGame(c *gin.Context) {
	input, err := ParseGame(c)
	lib.HandleError(err)
	game := c.MustGet("game").(*models.Game)
	err = mergo.Merge(game, input)
	lib.HandleError(err)
	game.Save()
	c.JSON(200, game)
}

func JoinGame(c *gin.Context) {
	game := c.MustGet("game").(*models.Game)
	var input models.Player
	err := json.NewDecoder(c.Request.Body).Decode(&input)
	lib.HandleError(err)
	game.AddPlayer(input.Name)
	game.Save()
	c.JSON(200, game)
}