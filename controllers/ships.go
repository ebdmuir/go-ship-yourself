package controllers

import (
	"ebdm.dev/battleships/v1/lib"
	"ebdm.dev/battleships/v1/models"
	"github.com/gin-gonic/gin"
	"github.com/imdario/mergo"
)

func UpdateShip(c *gin.Context) {
	game := c.MustGet("game").(*models.Game)
	shipId, _ := c.Params.Get("ship")

	ship, err := models.FetchShip(game, shipId)
	lib.HandleError(err)

	input, err := lib.ParseBody(c)
	lib.HandleError(err)
	input = input.(models.Ship)
	
	mergo.Merge(ship, input)

	for pindex, player := range game.Players {
		for iindex, item := range player.Ships {
			if(item.Id == ship.Id) {
				game.Players[pindex].Ships[iindex] = ship
			}
		}
	}
}