package main

import (
	"ebdm.dev/battleships/v1/controllers"
	"ebdm.dev/battleships/v1/middleware"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(gin.Recovery())
	r.GET("/games", controllers.GetAllGames)
	r.POST("/games/new", controllers.NewGame)
	games := r.Group("/games") 
	games.Use(middleware.FetchGame())
	{
		games.GET("/:game", controllers.GetGame)
		games.PUT("/:game", controllers.PutGame)
		games.POST("/:game/join", controllers.JoinGame)
		games.PUT("/:game/ship/:ship", controllers.UpdateShip)
	}
	r.Run()
}