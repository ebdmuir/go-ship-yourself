package lib

import (
	"fmt"

	scribble "github.com/nanobox-io/golang-scribble"
)

var DB *scribble.Driver 

func init() {
	var err error
	DB, err = scribble.New("db", nil)
	if err != nil {
		fmt.Println("Error", err)
	}
}

func SeedShipTypes() {
	
}