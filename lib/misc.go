package lib

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/tjarratt/babble"
)

func PrintObject(obj interface{}) {
	empJSON, err := json.MarshalIndent(obj, "", "  ")
	if err != nil {
		log.Fatalf(err.Error())
	}
	fmt.Printf("%s\n", string(empJSON))
}

func HandleError(err error) {
	if err != nil {
		fmt.Printf("%s\n", string(err.Error()))
	}
}

func ParseBody(c *gin.Context) (interface{}, error) {
	var body interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&body)
	PrintObject(body)
	return body, err
}

func RanId() string {
	babbler := babble.NewBabbler()
	return strings.ToLower(babbler.Babble())
}