package models

import (
	"strconv"
	"testing"
)

type TileCase struct {
	X int
	Y int
	Expected string
}

func TestTileGetID(t *testing.T) {
	cases := []TileCase {
		{ 1, 1, "A1" },
		{ 1, 2, "A2" },
		{ 2, 1, "B1" },
		{ 2, 2, "B2" },
	}
	for _, c := range cases {
		tile := Tile{
			X: c.X, Y: c.Y,
		}
		id := tile.GetId()
		if id != c.Expected {
			t.Log("ID should be "+c.Expected)
			t.Fail()
		}
	}
}

type BoardCase struct {
	Size int
	ExpectedTileLength int
}

func TestGetBoard(t *testing.T) {
	cases := []BoardCase {
		{ 2, 4 },
		{ 5, 25 },
	}
	for _, c := range cases {
		board := Board{Size: c.Size}
		tiles := board.GetTiles()
		if len(tiles) != c.ExpectedTileLength {
			t.Log("Board should be " + strconv.Itoa(c.ExpectedTileLength) + " tiles")
			t.Fail()
		}
	}
}