package models

import "strconv"

type Board struct {
	Size int `json:"size"`
	Ships []Ship `json:"ships"`
	Tiles []Tile `json:"tiles"`
}

type Tile struct {
	Id string `json:"id"`
	Ship *NullShip `json:"ship,omitempty"`
	X int `json:"x"`
	Y int `json:"y"`
}

func (tile *Tile) GetId() string {
	var X string
	switch tile.X {
	case 1:
		X = "A"
	case 2:
		X = "B"
	case 3:
		X = "C"
	case 4:
		X = "D"
	case 5:
		X = "E"
	case 6:
		X = "F"
	case 7:
		X = "G"
	case 8:
		X = "H"
	case 9:
		X = "I"
	case 10:
		X = "J"
	default:
		X = "?"
	}
	return X+strconv.Itoa(tile.Y)
}

func (board *Board) GetTiles() []Tile {
	tiles := []Tile{}
	for x := 0; x < board.Size; x++ {
		for y := 0; y < board.Size; y++ {
			tile := Tile{
				X: x+1,
				Y: y+1,
			}
			tile.Id = tile.GetId()
			tiles = append(tiles, tile)
		}
	}
	return tiles
}