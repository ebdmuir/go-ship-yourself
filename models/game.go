package models

import (
	"encoding/json"
	"fmt"
	"time"

	"ebdm.dev/battleships/v1/lib"
)

type Game struct {
	Id string `json:"id"`
	Name string `json:"name"`
	BoardSize int `json:"board_size"`
	Players []Player `json:"players"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	CurrentTurn string `json:"current_turn"`
}

func ( game *Game ) Save() {
	if err := lib.DB.Write("game", game.Id, game); err != nil {
		fmt.Println("Error", err)
	}
}

func (game *Game)AddPlayer(name string) {
	player := Player{}
	player.Name = name
	ships := []string{}
	ships = append(ships, "test")
	ships = append(ships, "test")

	if(len(ships) >= 1) {
		for i := 0; i < len(ships); i++ {
			player.AddToShipyard(ships[i])
		}
	}

	game.Players = append(game.Players, player)
}

func (game *Game)ValidateBoards() {
	for _, player := range game.Players {
		player.Board.Size = game.BoardSize
		player.Board.Tiles = player.Board.GetTiles()
	}
}

func NewGame() *Game {
	game := Game{}
	game.Id = lib.RanId()
	game.CreatedAt = time.Now()
	game.Save()

	return &game
}

func GetGame(id string) (*Game, error) {
	game := Game{}
	if err := lib.DB.Read("game", id, &game); err != nil {
		fmt.Println("Error", err)
		return &Game{}, err
	}
	return &game, nil
}

func GetAllGames() []Game {
	data, err := lib.DB.ReadAll("game")
	if err != nil {
		fmt.Println("Error", err)
	}
	games := []Game{}
	for _, f := range data {
		gameFound := Game{}
		if err := json.Unmarshal([]byte(f), &gameFound); err != nil {
			fmt.Println("Error", err)
		}
		games = append(games, gameFound)
	}

	return games
}