package models

import (
	"errors"
	"fmt"
	"strings"

	"ebdm.dev/battleships/v1/lib"
)

type NullShip struct {
	Valid bool
	Ship *Ship
}

type Ship struct {
	Id string `json:"id"`
	Type ShipType `json:"type"`
	Alive bool `json:"alive"`
	Position ShipPosition `json:"pos"`
	Health []bool `json:"health"`
}

type ShipPosition struct {
	OnBoard bool `json:"valid"`
	X int `json:"x,omitempty"`
	Y int `json:"y,omitempty"`
}

type ShipType struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Length int `json:"length"`
}

func (ship Ship) Save() {
	if err := lib.DB.Write("ship", ship.Id, ship); err != nil {
		fmt.Println("Error", err)
	}
}

func (ship Ship) IsAlive() bool {
	isAlive := true
	for _, item := range ship.Health {
		isAlive = isAlive && item
	}
	return isAlive
}

func (ship *Ship) CheckStatus() {
	ship.Alive = ship.IsAlive()
}

func (shiptype ShipType) Save() {
	if err := lib.DB.Write("shiptype", shiptype.Id, shiptype); err != nil {
		fmt.Println("Error", err)
	}
}

func FetchShipType(id string) (ShipType, error) {
	fmt.Println("Fetching: "+id)
	var shiptype ShipType
	if err := lib.DB.Read("shiptype", id, &shiptype); err != nil {
		fmt.Println("Found error while fetching")
		return ShipType{}, err
	} else {
		fmt.Println("Found type")
		lib.PrintObject(shiptype)
		return shiptype, nil
	}
}

func FetchShip(game *Game, id string) (*Ship, error) {
	fmt.Println("Fetching: "+id)
	for _, player := range game.Players {
		for _, item := range player.Ships {
			if(item.Id == id) {
				return item, nil
			}
		}
	}
	return &Ship{}, errors.New("Ship not found")
}

func AddShipType(name string, length int) {
	shiptype := ShipType{Name: name, Length: length}
	shiptype.Id = strings.ToLower(name)
	shiptype.Save()
}