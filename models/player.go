package models

import (
	"fmt"

	"ebdm.dev/battleships/v1/lib"
)

type Player struct {
	Name string `json:"name"`
	Board Board `json:"board"`
	Ships []*Ship `json:"ships"`
}

func (player *Player)AddToShipyard(shiptype string) {
	var err error
	ship := Ship{}
	ship.Id = lib.RanId()
	ship.Type, err = FetchShipType(shiptype)
	if err != nil {
		fmt.Printf("%s\n", string(err.Error()))
		AddShipType("test", 3)
	}
	lib.PrintObject(ship)
	fmt.Println(ship.Type.Length)
	if(ship.Type.Length >= 1) {
		for i:=0; i<ship.Type.Length; i++ {
			ship.Health = append(ship.Health, true)
		}
	}
	ship.Alive = ship.IsAlive()
	
	player.Ships = append(player.Ships, &ship)
}

// AddShip(player Player, type string, x int, y int) {
// 	player.
// }